# COMPUTER GRAPHICS FINAL PROJECT




## wide-angle-study
wide angle study with pbrt

**To view the rendered images from the inside and outside scenes, please change to the branches** 




**how to start the project**
Go into the Release directory and do


```
./ex2.ps1 $arg1 $arg2
```
For example

```
./ex2.ps1 photography ../Results/firstphoto
```

$arg1 is the name of the file .pbrt
$arg2 is the output.png file location
